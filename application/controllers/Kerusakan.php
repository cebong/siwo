<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Kerusakan extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_kerusakan');
		}
	
		function tambah(){
			$jenis = $this->input->post('jenis');
			$lokasi = $this->input->post('lokasi');
			$keterangan = $this->input->post('keterangan');
			$user = $this->session->userdata('id_user');

			$data = array(
				'id_user' => $user,
				'id_jenis' => $jenis,
				'id_lokasi' => $lokasi,
			);
			$id_order = $this->m_kerusakan->create_kerusakan($data,'work_order');

			$detail = array(
				'id_order' => $id_order,
				'keterangan' => $keterangan,
				'status' => "Belum Dikerjakan",
			);
			$this->m_kerusakan->create_detail($detail,'detail_work_order');
			redirect('staff/kerusakan');
		}

		function lihat_kerusakan($id){
			$where = array('detail_work_order.id_order' => $id);
			$data = array(
				'kerusakan' => $this->m_kerusakan->look_crash($where)->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/kerusakan/teknisi/lihat',$data);
			$this->load->view('dashboard/footer');
		}

		function diproses($id){
			$where = array('id_order' => $id);
			$user = $this->session->userdata('id_user');

			$data = array(
				'id_user' => $user,
				'status' => "Sedang Diproses",
			);

			$this->m_kerusakan->proses($where,$data,'detail_work_order');
			redirect('teknisi');
		}

		function selesai($id){
			$where = array('id_order' => $id);
			$data = array(
				'status' => "Sudah",
			);

			$this->m_kerusakan->clear($where,$data,'detail_work_order');
			redirect('teknisi');
		}
	
	}
	
	/* End of file Kerusakan.php */
	/* Location: ./application/controllers/Kerusakan.php */
?>
<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class User extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_user');
			$this->load->model('m_jabatan');
		}
	
		function tambah(){
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$nomer = $this->input->post('nomer');
			$jk = $this->input->post('jk');
			$jabatan = $this->input->post('jabatan');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');
			$hak = $this->input->post('hak');
			$admin = $this->input->post('admin');
			$staff = $this->input->post('staff');
			$teknisi = $this->input->post('teknisi');
			$manager = $this->input->post('manager');

			$data = array(
				'nama_user' => $nama,
				'alamat' => $alamat,
				'telp_user' => $nomer,
				'jenis_kelamin' => $jk,
				'id_jabatan' => $jabatan,
				'username' => $username,
				'password' => md5($password),
				'email' => $email,
				'path_user' => "default.png",
				'akses_default' => $hak,
				'admin' => $admin,
				'staff' => $staff,
				'teknisi' => $teknisi,
				'manager' => $manager,
			);

			$this->m_user->create($data,'user');
			redirect('admin/user');
		}

		function edit($id){
			$where = array('id_user' => $id);
			$data = array(
				'jabatan' => $this->m_jabatan->list_jabatan()->result(),
				'user' => $this->m_user->get($where,'user')->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/user/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$nomer = $this->input->post('nomer');
			$jk = $this->input->post('jk');
			$jabatan = $this->input->post('jabatan');
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$admin = $this->input->post('admin');
			$staff = $this->input->post('staff');
			$teknisi = $this->input->post('teknisi');
			$manager = $this->input->post('manager');

			// var_dump($hak);

			$where = array('id_user' => $id);
			$data = array(
				'nama_user' => $nama,
				'alamat' => $alamat,
				'telp_user' => $nomer,
				'jenis_kelamin' => $jk,
				'id_jabatan' => $jabatan,
				'username' => $username,
				'email' => $email,
				'admin' => $admin,
				'staff' => $staff,
				'teknisi' => $teknisi,
				'manager' => $manager,
			);

			$this->m_user->replace($where,$data,'user');
			redirect('admin/user');
		}

		function hapus($id){
			$where = array('id_user' => $id);
			$this->m_user->trash($where,'user');
			redirect('admin/user');
		}

		function pencarian(){
			$keyword = $this->input->post('keyword');
			$data = array('user' => $this->m_user->search($keyword,'user')->result());
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/user/index',$data);
			$this->load->view('dashboard/footer');
		}
	}
	
	/* End of file User.php */
	/* Location: ./application/controllers/User.php */
?>
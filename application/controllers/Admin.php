<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Admin extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_jabatan');
			$this->load->model('m_jenis');
			$this->load->model('m_user');
			$this->load->model('m_lokasi');
			$this->load->model('m_kerusakan');
		}
	
		function jabatan(){
			$data['jabatan'] = $this->m_jabatan->list_jabatan()->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/jabatan/index',$data);
			$this->load->view('dashboard/footer');
		}

		function jenis_order(){
			$data['jenis'] = $this->m_jenis->list_jenis()->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/jenis/index',$data);
			$this->load->view('dashboard/footer');
		}

		function lokasi(){
			$data['lokasi'] = $this->m_lokasi->list_lokasi()->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/lokasi/index',$data);
			$this->load->view('dashboard/footer');
		}

		function user(){
			$data = array(
				'jabatan' => $this->m_jabatan->list_jabatan()->result(),
				'user' => $this->m_user->list_user()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/user/index',$data);
			$this->load->view('dashboard/footer');
		}

		public function export_excel(){
           $data = array(
           	'title' => 'Daftar Karyawan Single Fin',
            'user' => $this->m_user->list_user()->result(),
       		);
 
           $this->load->view('dashboard/user/excel',$data);
      	}
      	
		function kerusakan_proses(){
			$data = array(
				'kerusakan' => $this->m_kerusakan->list_proses()->result(),
				'teknisi' => $this->m_kerusakan->daftar_teknisi()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/kerusakan/admin/proses',$data);
			$this->load->view('dashboard/footer');
		}

		function kerusakan_selesai(){
			$data = array(
				'kerusakan' => $this->m_kerusakan->list_selesai()->result(),
				'teknisi' => $this->m_kerusakan->daftar_teknisi()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/kerusakan/admin/selesai',$data);
			$this->load->view('dashboard/footer');
		}
	
	}
	
	/* End of file Admin.php */
	/* Location: ./application/controllers/Admin.php */
?>
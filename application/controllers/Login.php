<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Login extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_login');
			$this->load->helper('url');
		}
	
		public function index()
		{
			$this->load->view('login');
		}

		public function aksi_login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$where = array(
				'username' => $username,
				'password' => md5($password),
			);

			$cek = $this->m_login->cek_login($where);
			if($cek->num_rows() > 0){
				$row = $cek->row();
				$data_session = array(
					'logged_in' => TRUE,
					'id_user' => $row->id_user,
					'nama_user' => $row->nama_user,
					'alamat' => $row->alamat,
					'telp_user' => $row->telp_user,
					'jenis_kelamin' => $row->jenis_kelamin,
					//'jabatan' => $row->nama_jabatan,
					'username' => $username,
					'email' => $row->email,
					'path_user' => $row->path_user,
					'akses_default' => $row->akses_default,
					'admin' => $row->admin,
					'staff' => $row->staff,
					'teknisi' => $row->teknisi,
					'manager' => $row->manager,
				);
				$this->session->set_userdata($data_session);
				redirect(site_url('dashboard'));
			}
			else{
				echo "Username dan Password Salah";
			}
		}

		function rubah_akses($akses){
			$rubah = array('akses_default' => $akses);
			$this->session->set_userdata($rubah);
			redirect('dashboard');
			// $id = $this->session->userdata('id_user');
			// $cek = $this->session->m_login->cek_akses($id);
			// if($cek)
		}

		function logout(){
			$this->session->sess_destroy();
			redirect('login');
		}
	
	}
	
	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
?>
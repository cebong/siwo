<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Jenis extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_jenis');
		}
	
		function tambah(){
			$jenis = $this->input->post('jenis');

			$data = array('nama_jenis' => $jenis);
			$this->m_jenis->create($data,'jenis');
			redirect('admin/jenis_order');
		}

		function edit($id){
			$where = array('id_jenis' => $id);
			$data['jenis'] = $this->m_jenis->get($where,'jenis')->result();

			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/jenis/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$jenis = $this->input->post('jenis');

			$data = array(
				'nama_jenis' => $jenis,
			);
			$where = array('id_jenis' => $id);

			$this->m_jenis->replace($where,$data,'jenis');
			redirect('admin/jenis_order');
		}

		function hapus($id){
			$where = array('id_jenis' => $id);
			$this->m_jenis->trash($where,'jenis');
			redirect('admin/jenis_order');
		}
	
	}
	
	/* End of file Jenis.php */
	/* Location: ./application/controllers/Jenis.php */
?>
<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Lokasi extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_lokasi');
		}
	
		function tambah(){
			$data = array('nama_lokasi' => $this->input->post('lokasi'));
			$this->m_lokasi->insert($data,'lokasi');
			redirect('admin/lokasi');
		}

		function edit($id){
			$where = array('id_lokasi' => $id);
			$data = array('lokasi' => $this->m_lokasi->get($where,'lokasi')->result());
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/lokasi/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$lokasi = $this->input->post('lokasi');

			$where = array('id_lokasi' => $id);
			$data = array('nama_lokasi' => $lokasi);

			$this->m_lokasi->replace($where,$data,'lokasi');
			redirect('admin/lokasi');
		}

		function hapus($id){
			$where = array('id_lokasi' => $id);
			$this->m_lokasi->trash($where,'lokasi');
			redirect('admin/lokasi');
		}
	
	}
	
	/* End of file Lokasi.php */
	/* Location: ./application/controllers/Lokasi.php */
?>
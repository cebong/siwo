<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_kerusakan');
		$this->load->model('m_lokasi');
		$this->load->model('m_jenis');
	}

	function kerusakan(){
		$data = array(
			'kerusakan' => $this->m_kerusakan->list_kerusakan_staff()->result(),
			'lokasi' => $this->m_lokasi->list_lokasi()->result(),
			'jenis' => $this->m_jenis->list_jenis()->result(),
		);
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/kerusakan/staff/index',$data);
		$this->load->view('dashboard/footer');
	}

}

/* End of file Staff.php */
/* Location: ./application/controllers/Staff.php */ 
?>
<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Laporan extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_laporan');
			$this->load->library('pdf');
		}

		function user(){
			$user = $this->input->post('user');

			$where = array('id_jabatan' => $user);

			 $pdf = new FPDF('p','mm','A4');
		        $pdf->SetTitle('Daftar Pegawai Single Fin');
		        
		        $pdf->AliasNbPages();
		        $pdf->AddPage();
		        // setting jenis font yang akan digunakan
		        $pdf->SetFont('Arial','B',16);
		        // mencetak string 
		        // $pdf->Image('assets/images/logo.png',20,10,20,20);
		        $pdf->Cell(190,7,'Daftar Pegawai Single Fin',0,1,'C');
		        $pdf->SetFont('Arial','B',12);
		        // Memberikan space kebawah agar tidak terlalu rapat
		        $pdf->Cell(10,7,'',0,1);
		        $pdf->Cell(10,7,'',0,1);
		        $pdf->Cell(10,7,'',0,1);
		        $pdf->SetFillColor(17,56,256);
		        $pdf->SetFont('Arial','B',10);
		        $pdf->Cell(10,7,'',0,0);
		        $pdf->Cell(50,6,'Nama',1,0,'C',TRUE);
		        $pdf->Cell(15,6,'JK',1,0,'C',TRUE);
		        $pdf->Cell(27,6,'No.Hp',1,0,'C',TRUE);
		        $pdf->Cell(50,6,'Email',1,0,'C',TRUE);
		        $pdf->Cell(25,6,'Alamat',1,1,'C',TRUE);
		        $pdf->SetFont('Arial','',10);
		        
		        $peserta = $this->m_laporan->data_user($where,'user')->result();
		        foreach ($peserta as $row){
		            $pdf->Cell(10,7,'',0,0);
		            $pdf->Cell(50,6,$row->nama_user,1,0);
		            $pdf->Cell(15,6,$row->jenis_kelamin,1,0,'C');
		            $pdf->Cell(27,6,$row->telp_user,1,0);
		            $pdf->Cell(50,6,$row->email,1,0);
		            $pdf->Cell(25,6,$row->alamat,1,1);
		        }

		        // Go to 1.5 cm from bottom
		        $pdf->SetY(15);
		        $pdf->SetFont('Arial','I',8);
		        $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' Single Fin Bali 2018',0,0,'C');

		        $pdf->Output();
		    }

		    function bug(){
			$status = $this->input->post('status');

			$where = array('status' => $status);

			 $pdf = new FPDF('p','mm','A4');
		        $pdf->SetTitle('Daftar Kerusakan Single Fin');
		        
		        $pdf->AliasNbPages();
		        $pdf->AddPage();
		        // setting jenis font yang akan digunakan
		        $pdf->SetFont('Arial','B',16);
		        // mencetak string 
		        // $pdf->Image('assets/images/logo.png',20,10,20,20);
		        $pdf->Cell(190,7,'Daftar Pegawai Single Fin',0,1,'C');
		        $pdf->SetFont('Arial','B',12);
		        // Memberikan space kebawah agar tidak terlalu rapat
		        $pdf->Cell(10,7,'',0,1);
		        $pdf->Cell(10,7,'',0,1);
		        $pdf->Cell(10,7,'',0,1);
		        $pdf->SetFillColor(17,56,256);
		        $pdf->SetFont('Arial','B',10);
		        $pdf->Cell(10,7,'',0,0);
		        $pdf->Cell(40,6,'Tanggal order',1,0,'C',TRUE);
		        $pdf->Cell(25,6,'Lokasi',1,0,'C',TRUE);
		        $pdf->Cell(37,6,'Jenis Kerusakan',1,0,'C',TRUE);
		        $pdf->Cell(50,6,'Pelapor',1,1,'C',TRUE);
		        $pdf->SetFont('Arial','',10);
		        
		        $peserta = $this->m_laporan->data_bug($where,'detail_work_order')->result();
		        foreach ($peserta as $row){
		            $pdf->Cell(10,7,'',0,0);
		            $pdf->Cell(40,6,date("d/F/Y",strtotime($row->tgl_order)),1,0);
		            $pdf->Cell(25,6,$row->nama_lokasi,1,0,'C');
		            $pdf->Cell(37,6,$row->nama_jenis,1,0);
		            $pdf->Cell(50,6,$row->nama_user,1,1);
		        }

		        // Go to 1.5 cm from bottom
		        $pdf->SetY(15);
		        $pdf->SetFont('Arial','I',8);
		        $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' Single Fin Bali 2018',0,0,'C');

		        $pdf->Output();
		    }
	
	}
	
	/* End of file Laporan.php */
	/* Location: ./application/controllers/Laporan.php */
?>
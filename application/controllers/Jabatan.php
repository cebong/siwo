<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Jabatan extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_jabatan');
		}
	
		function tambah(){
			$jabatan = $this->input->post('jabatan');
			$data = array('nama_jabatan' => $jabatan);
			$this->m_jabatan->create($data,'jabatan');
			redirect('admin/jabatan');
		}

		function edit($id){
			$where = array('id_jabatan' => $id);
			$data['jabatan'] = $this->m_jabatan->get($where,'jabatan')->result();

			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/jabatan/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$jabatan = $this->input->post('jabatan');

			$where = array('id_jabatan' => $id);
			$data = array('nama_jabatan' => $jabatan);

			$this->m_jabatan->replace($where,$data,'jabatan');
			redirect('admin/jabatan');
		}

		function hapus($id){
			$where = array('id_jabatan' => $id);
			$this->m_jabatan->trash($where,'jabatan');
			redirect('admin/jabatan');
		}
	
	}
	
	/* End of file Jabatan.php */
	/* Location: ./application/controllers/Jabatan.php */
?>
<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Teknisi extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_kerusakan');
		}
	
		public function index()
		{
			$data = array(
				'kerusakan' => $this->m_kerusakan->list_kerusakan()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/kerusakan/teknisi/index',$data);
			$this->load->view('dashboard/footer');
		}
	
	}
	
	/* End of file Teknisi.php */
	/* Location: ./application/controllers/Teknisi.php */
?>
 <?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
 
 <table border="1" width="100%">
 
      <thead>
 
           <tr>
 
                <th>Nama user</th>
 
                <th>No. Telepon</th>
 
                <th>Email</th>

                <th>Jabatan</th>
 
           </tr>
 
      </thead>
 
      <tbody>
 
           <?php $i=1; foreach($user as $u) { ?>
 
           <tr>
 
                <td><?php echo $u->nama_user; ?></td>
                <td><?php echo $u->telp_user; ?></td>
                <td><?php echo $u->email; ?></td>
                <td><?php echo $u->nama_jabatan ?></td>
 
           </tr>
 
           <?php $i++; } ?>
 
      </tbody>
 
 </table>
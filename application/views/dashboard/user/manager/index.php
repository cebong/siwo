 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Manager Single Fin
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/data_manager') ?>">Data Manager</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah Staff</a>
              <table class="table table-bordered">
                <tr>
                  <th class="text-center">Nama User</th>
                  <th class="text-center">Telp.</th>
                  <th class="text-center">Username</th>
                  <th class="text-center">E-mail</th>
                  <th class="text-center">jabatan</th>
                  <th class="text-center">Action</th>
                </tr>
                <?php foreach($staff as $s){ ?>
                <tr>
                  <td><?php echo $s->nama_user ?></td>
                  <td><?php echo $s->telp_user ?></td>
                  <td><?php echo $s->username ?></td>
                  <td><?php echo $s->email ?></td>
                  <td><?php echo $s->nama_jabatan ?></td>
                  <td class="text-center">
                    <a href="<?php echo site_url('user/edit_manager/'.$s->id_user) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                    <a href="<?php echo site_url('user/hapus_manager/'.$s->id_user) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                  </td>
                </tr>
              <?php } ?>
              </table>
            </div>
          </div>
        </div>
      </div>  
    <section>
    <!-- /.content -->
    <!-- Modal -->
          <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Form Tambah Staff</h4>
                </div>
                <div class="modal-body">
                  <form action="<?php echo site_url('user/tambah_manager') ?>" method="post">
                    <div class="form-group">
                      <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" name="alamat" placeholder="Alamat"></textarea>
                    </div>
                    <div class="form-group">
                      <input type="text" name="nomer" class="form-control" placeholder="No Telepon">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Jenis Kelamin :</label>
                      <input type="radio" name="jk" class="flat-red" value="L">&nbsp;<i class="fa fa-male"></i>&nbsp;Laki-Laki
                      <input type="radio" name="jk" class="flat-red" value="P">&nbsp;<i class="fa fa-female"></i>&nbsp;Perempuan
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="jabatan">
                        <option>-- Pilih Jabatan --</option>
                        <?php foreach($jabatan as $j){ ?>
                          <option value="<?php echo $j->id_jabatan ?>"><?php echo $j->nama_jabatan ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <input type="text" name="username" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <input type="email" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <input type="checkbox" name="hak" class="minimal" value="Admin">&nbsp;Admin
                      <input type="checkbox" name="hak" class="minimal" value="Staff">&nbsp;Staff
                      <input type="checkbox" name="hak" class="minimal" value="Teknisi">&nbsp;Teknisi
                      <input type="checkbox" name="hak" class="minimal" value="Manager">&nbsp;Manager
                      <input type="checkbox" name="hak" class="minimal" value="Leader">&nbsp;Leader
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-info"><i class="fa fa-save"></i>&nbsp;Register</button>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
  </div>
  <!-- /.content-wrapper -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Form Edit Data Staff
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/data_staff') ?>">Data Staff</a></li>
            <li><a href="#">Edit</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"></div>
                    <div class="box-body">
                        <?php foreach($user as $u){ ?>
                        <form action="<?php echo site_url('user/update') ?>" method="post">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="<?php echo $u->id_user ?>">
                                <input type="text" class="form-control" name="nama" value="<?php echo $u->nama_user ?>">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="alamat"><?php echo $u->alamat ?></textarea>
                            </div>
                            <div class="form-group">
                                <input type="text" name="nomer" class="form-control" value="<?php echo $u->telp_user ?>">
                            </div>
                            <div class="form-group">
                                <input type="radio" name="jk" class="minimal" value="L" <?php if($u->jenis_kelamin == "L"){echo "checked";} ?>>Laki-Laki
                                <input type="radio" name="jk" class="minimal" value="P" <?php if($u->jenis_kelamin == "P"){echo "checked";} ?>>Perempuan
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="jabatan">
                                    <option>-- Pilih Jabatan --</option>
                                    <?php foreach($jabatan as $j){ ?>
                                    <option value="<?php echo $j->id_jabatan ?>" <?php if($u->id_jabatan == $j->id_jabatan){echo "selected";} ?>><?php echo $j->nama_jabatan ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" value="<?php echo $u->username ?>">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" value="<?php echo $u->email ?>">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="admin" class="minimal" value="1" <?php if($u->akses_default == "Admin" || $u->admin==1){echo "checked";} ?>>Admin
                                <input type="checkbox" name="staff" class="minimal" value="1" <?php if($u->akses_default == "Staff" || $u->staff==1){echo "checked";} ?>>Staff
                                <input type="checkbox" name="teknisi" class="minimal" value="1" <?php if($u->akses_default == "Teknisi" || $u->teknisi==1){echo "checked";} ?>>Teknisi
                                <input type="checkbox" name="manager" class="minimal" value="1" <?php if($u->akses_default == "Manager" || $u->manager==1){echo "checked";} ?>>Manager
                            </div>
                            <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i>&nbsp;Update</button>
                            <a href="<?php echo site_url('admin/user') ?>" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Batal</a>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
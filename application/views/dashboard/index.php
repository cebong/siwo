<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if($this->session->userdata('akses_default')=="Admin"){ ?>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('user');
                  $this->db->not_like('admin','1');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Total User</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('lokasi');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Lokasi</p>
            </div>
            <div class="icon">
              <i class="fa fa-map-marker"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('work_order');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Order Masuk</p>
            </div>
            <div class="icon">
              <i class="fa fa-wrench"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    <?php } ?>
    <?php if($this->session->userdata('akses_default') == "Teknisi"){ ?>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('work_order');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Order Masuk</p>
            </div>
            <div class="icon">
              <i class="fa fa-wrench"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('detail_work_order');
                  $this->db->where('id_user',$this->session->userdata('id_user'));
                  $this->db->where('status',"Sudah");
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Order Sudah Dikerjakan</p>
            </div>
            <div class="icon">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIWO | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dashboard/plugins/iCheck/all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url('dashboard') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SIWO</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SiWo</b>Dashboard</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i></a>
            <ul class="dropdown-menu">
              <?php if($this->session->userdata('admin')==1){ ?>
              <li><a href="<?php echo site_url('login/rubah_akses/'."Admin") ?>"><i class="fa fa-user-secret"></i>&nbsp;Admin</a></li>
              <?php } ?>
              <?php if($this->session->userdata('staff')==1){ ?>
              <li><a href="<?php echo site_url('login/rubah_akses/'."Staff") ?>"><i style="color: #0639F9" class="fa fa-user"></i>&nbsp;Staff</a></li>
              <?php } ?>
              <?php if($this->session->userdata('teknisi')==1){ ?>
              <li><a href="<?php echo site_url('login/rubah_akses/'."Teknisi") ?>"><i style="color: #0CFA0C" class="fa fa-user"></i>&nbsp;Teknisi</a></li>
              <?php } ?>
              <?php if($this->session->userdata('manager')==1){ ?>
              <li><a href="<?php echo site_url('login/rubah_akses/'."Manager") ?>"><i style="color: #F8FB05" class="fa fa-user"></i>&nbsp;Manager</a></li>
              <?php } ?>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/images/dp/'.$this->session->userdata('path_user')) ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('nama_user') ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/images/dp/'.$this->session->userdata('path_user')) ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('nama_user') ?>
                  <small>Hak Akses : <?php echo $this->session->userdata('email') ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('login/logout') ?>" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/images/dp/'.$this->session->userdata('path_user')) ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username') ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree" id="nav">
        <li class="header">MAIN NAVIGATION</li>
        <?php if($this->session->userdata('akses_default')=="Admin"){ ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i>
            <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('admin/jabatan') ?>"><i class="fa fa-circle-o"></i> Jabatan</a></li>
            <li><a href="<?php echo site_url('admin/jenis_order') ?>"><i class="fa fa-circle-o"></i> Jenis Order</a></li>
            <li><a href="<?php echo site_url('admin/lokasi') ?>"><i class="fa fa-circle-o"></i> Lokasi</a></li>
          </ul>
        </li>
        <li>
          <a href="<?php echo site_url('admin/user') ?>">
            <i class="fa fa-users"></i>
            <span>Data User</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-bug"></i>
            <span>Data Kerusakan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('list_kerusakan') ?>"><i class="fa fa-circle-o"></i> Daftar Kerusakan</a></li>
            <li><a href="<?php echo site_url('admin/kerusakan_proses') ?>"><i class="fa fa-circle-o"></i> Daftar Kerusakan Diproses</a></li>
            <li><a href="<?php echo site_url('admin/kerusakan_selesai') ?>"><i class="fa fa-circle-o"></i> Daftar Kerusakan Selesai</a></li>
          </ul>
        </li>
        <?php } ?>
        <?php if($this->session->userdata('akses_default') == "Staff"){ ?>
        <li>
          <a href="<?php echo site_url('staff/kerusakan') ?>"><i class="fa fa-bug"></i>&nbsp;Lapor Kerusakan</a>
        </li>
        <?php } ?>
        <?php if($this->session->userdata('akses_default') == "Teknisi"){ ?>
        <li>
          <a href="<?php echo site_url('teknisi') ?>"><i class="fa fa-bug"></i>&nbsp;Daftar Kerusakan</a>
        </li>
        <?php } ?>
        <?php if($this->session->userdata('akses_default') == "Manager"){ ?>
        <li>
          <a href="<?php echo site_url('kerusakan') ?>"><i class="fa fa-exclamation"></i>&nbsp;Lapor Kerusakan</a>
        </li>
        <li>
          <a href="<?php echo site_url('teknisi') ?>"><i class="fa fa-bug"></i>&nbsp;Daftar Kerusakan</a>
        </li>
        <li>
          <a href="<?php echo site_url('dashboard/laporan') ?>"><i class="fa fa-file-pdf-o"></i>&nbsp;Rekap Laporan</a>
        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
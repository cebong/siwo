<div class="content-wrapper">
	<section class="content-header">
		<h1>Rekap Laporan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="text-center">
									<div class="form-group">
										<i class="fa fa-4x fa-users"></i><small><i class="fa fa-2x fa-file-pdf-o"></i></small>
									</div>
									<div class="form-group">
										<a href="#" data-toggle="modal" data-target="#user" class="btn btn-info btn-flat">Cetak</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="text-center">
									<div class="form-group">
										<i class="fa fa-4x fa-bug"></i><small><i class="fa fa-2x fa-file-pdf-o"></i></small>
									</div>
									<div class="form-group">
										<a href="#" data-toggle="modal" data-target="#bug" class="btn btn-info btn-flat">Cetak</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Cetak -->
	      <div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog" role="document">
	          <div class="modal-content">
	            <div class="modal-header">
	              <h3 id="myModalLabel">Lapor</h3>
	              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">×</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              <form class="form-horizontal templatemo-create-account templatemo-container-modal" action="<?php echo site_url('laporan/user');?>" method="post" target="_blank">
	                <div class="form-group">
	                	<div class="col-md-12">
		                	<label class="control-label">Pilih Pegawai :</label>
		                	<select class="form-control" name="user">
		                		<option value="1">Admin</option>
		                		<option value="2">Manager</option>
		                		<option value="3">Teknisi</option>
		                		<option value="4">Chef</option>
		                		<option value="5">Kasir</option>
		                	</select>
	                	</div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-12 text-center">
	                		<button type="submit" class="btn btn-success btn-flat"><i class="fa fa-file-pdf-o"></i>Generate</button>
	                	</div>
	                </div>
	              </form>
	            </div>
	          </div>
	        </div>
	      </div>
	      <!-- Modal Cetak -->
	      <!-- Modal Cetak -->
	      <div class="modal fade" id="bug" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog" role="document">
	          <div class="modal-content">
	            <div class="modal-header">
	              <h3 id="myModalLabel">Lapor</h3>
	              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">×</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              <form class="form-horizontal templatemo-create-account templatemo-container-modal" action="<?php echo site_url('laporan/bug');?>" method="post" target="_blank">
	                <div class="form-group">
	                	<div class="col-md-12">
		                	<label class="control-label">Pilih Status Bug :</label>
		                	<select class="form-control" name="status">
		                		<option value="Sudah">Sudah Dikerjakan</option>
		                		<option value="Sedang Diproses">Diproses</option>
		                		<option value="Belum Dikerjakan">Belum Dikerjakan</option>
		                	</select>
	                	</div>
	                </div>
	                <div class="form-group">
	                	<div class="col-md-12 text-center">
	                		<button type="submit" class="btn btn-success btn-flat"><i class="fa fa-file-pdf-o"></i>Generate</button>
	                	</div>
	                </div>
	              </form>
	            </div>
	          </div>
	        </div>
	      </div>
	      <!-- Modal Cetak -->
	</section>
</div>
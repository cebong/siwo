<div class="content-wrapper">
	<section class="content-header">
		<h1>Form Edit</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="<?php echo site_url('admin/lokasi') ?>">Lokasi</a></li>
			<li class="active"><a href="#">Edit</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php foreach($lokasi as $l){ ?>
						<form action="<?php echo site_url('lokasi/update') ?>" method="post">
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" value="<?php echo $l->id_lokasi ?>">
								<input type="text" name="lokasi" class="form-control" placeholder="Nama Lokasi" value="<?php echo $l->nama_lokasi ?>">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-warning">Update</button>
							</div>
						</form>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
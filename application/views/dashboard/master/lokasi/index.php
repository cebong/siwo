<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Lokasi<small>Single Fin Bali</small></h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('admin/lokasi') ?>"></a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-body">
						<form action="<?php echo site_url('lokasi/tambah') ?>" method="post">
							<div class="form-group">
								<input type="text" name="lokasi" class="form-control" placeholder="Nama Lokasi">
							</div>	
							<div class="form-group">
								<button type="submit" class="btn btn-info"><i class="fa fa-save"></i>&nbsp;Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Nama Lokasi</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<?php $no=1; foreach($lokasi as $l){ ?>
							<tbody>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $l->nama_lokasi ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('lokasi/edit/'.$l->id_lokasi) ?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
										<a href="<?php echo site_url('lokasi/hapus/'.$l->id_lokasi) ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							</tbody>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
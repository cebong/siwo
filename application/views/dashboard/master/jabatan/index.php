 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kabupaten
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/jabatan') ?>">Jabatan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-body">
              <form action="<?php echo site_url('jabatan/tambah') ?>" method="post">
                <div class="form-group">
                <input type="text" name="jabatan" class="form-control" placeholder="Nama Jabatan">
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Simpan</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Kabupaten</th>
                  <th>Action</th>
                </tr>
                <?php $no = 1; foreach($jabatan as $j){ ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $j->nama_jabatan ?></td>
                  <td class="text-center">
                    <a href="<?php echo site_url('jabatan/edit/'.$j->id_jabatan) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                    <a href="<?php echo site_url('jabatan/hapus/'.$j->id_jabatan) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                  </td>
                </tr>
              <?php } ?>
              </table>
            </div>
          </div>
        </div>
      </div>  
    <section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
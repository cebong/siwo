<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Form Edit Data Jabatan
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/jabatan') ?>">Jabatan</a></li>
            <li><a href="#">Edit</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"></div>
                    <div class="box-body">
                        <?php foreach($jabatan as $j){ ?>
                        <form action="<?php echo site_url('jabatan/update') ?>" method="post">
                            <input type="hidden" class="form-control" name="id" value="<?php echo $j->id_jabatan ?>">
                            <input type="text" class="form-control" name="jabatan" value="<?php echo $j->nama_jabatan ?>">
                            <br>
                            <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i>&nbsp;Update</button>
                            <a href="<?php echo site_url('admin/jabatan') ?>" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Batal</a>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
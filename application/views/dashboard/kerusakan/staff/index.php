<div class="content-wrapper">
	<section class="content-header">
		<h1>Data Kerusakan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;Lapor</a>
					</div>
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Tanggal Lapor</th>
									<th class="text-center">Jenis Order</th>
									<th class="text-center">Lokasi</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<?php $no=1; foreach($kerusakan as $k){ ?>
							<tbody>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $k->tgl_order ?></td>
									<td><?php echo $k->nama_jenis ?></td>
									<td><?php echo $k->nama_lokasi ?></td>
									<td class="text-center"><?php echo $k->status ?></td>
									<td class="text-center">
										<a href="#" class="btn btn-success"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							</tbody>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Tambah -->
      <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 id="myModalLabel">Lapor</h3>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal templatemo-create-account templatemo-container-modal" action="<?php echo site_url('kerusakan/tambah');?>" method="post">
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label">Jenis Kerusakan</label>
                    <select class="form-control" name="jenis">
                    	<option>-- Pilih Jenis Kerusakan --</option>
                    	<?php foreach($jenis as $j){ ?>
                    	<option value="<?php echo $j->id_jenis ?>"><?php echo $j->nama_jenis ?></option>
                    	<?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                	<div class="col-md-12">
                		<label class="control-label">Lokasi</label>
                		<select class="form-control" name="lokasi">
                			<option>-- Pilih Lokasi Kerusakan --</option>
                			<?php foreach($lokasi as $l){ ?>
                			<option value="<?php echo $l->id_lokasi ?>"><?php echo $l->nama_lokasi ?></option>
                			<?php } ?>
                		</select>
                	</div>
                </div>
                <div class="form-group">
                	<div class="col-md-12">
                		<label class="control-label">Keterangan</label>
                		<textarea class="form-control" name="keterangan" placeholder="Keterangan kerusakan"></textarea>
                	</div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                    <button class="btn btn-warning" type="button" data-dismiss="modal" aria-label="Close">
                      Tutup
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
	</section>
</div>
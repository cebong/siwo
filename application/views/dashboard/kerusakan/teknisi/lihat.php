<div class="content-wrapper">
	<section class="content-header">
		<h1>Detail Kerusakan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php foreach($kerusakan as $k){ ?>
						<div class="form-group">
							<label class="control-label">Tanggal Order</label>
							<input type="text" name="order" class="form-control" value="<?php echo $k->tgl_order ?>" readonly>	
						</div>
						<div class="form-group">
							<label class="control-label">Jenis Order</label>
							<input type="text" name="jenis" class="form-control" value="<?php echo $k->nama_jenis ?>" readonly>
						</div>
						<div class="form-group">
							<label class="control-label">Lokasi</label>
							<input type="text" name="lokasi" class="form-control" value="<?php echo $k->nama_lokasi ?>" readonly>
						</div>
						<div class="form-group">
							<label class="control-label">Pelapor</label>
							<input type="text" name="pelapor" class="form-control" value="<?php echo $k->nama_user ?>" readonly>
						</div>
						<div class="form-group">
							<label class="control-label">Keteranagn</label>
							<textarea class="form-control" name="keterangan" readonly><?php echo $k->keterangan ?></textarea>
						</div>
						<div class="form-group">
							<label class="control-label">Status</label>
							<input type="text" name="status" class="form-control" value="<?php echo $k->status ?>" readonly>
						</div>
						<div class="form-group">
							<a href="<?php echo site_url('teknisi') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
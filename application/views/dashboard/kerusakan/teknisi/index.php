<div class="content-wrapper">
	<section class="content-header">
		<h1>List Work Order</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Tanggal Order</th>
									<th class="text-center">Jenis</th>
									<th class="text-center">Lokasi</th>
									<th class="text-center">Pelapor</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<?php $no=1; foreach($kerusakan as $k){ ?>
							<tbody>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $k->tgl_order ?></td>
									<td class="text-center"><?php echo $k->nama_jenis ?></td>
									<td><?php echo $k->nama_lokasi ?></td>
									<td><?php echo $k->nama_user ?></td>
									<td class="text-center"><?php echo $k->status ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('kerusakan/lihat_kerusakan/'.$k->id_order) ?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
										<a href="<?php echo site_url('kerusakan/diproses/'.$k->id_order) ?>" class="btn btn-warning btn-xs" <?php if($k->status=="Sedang Diproses" || $k->status=="Sudah"){echo "disabled";} ?>><i class="fa fa-clock-o"></i></a>
										<a href="<?php echo site_url('kerusakan/selesai/'.$k->id_order) ?>" class="btn btn-success btn-xs" <?php if($k->status=="Sudah"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
									</td>
								</tr>
							</tbody>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
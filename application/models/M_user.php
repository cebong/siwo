<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_user extends CI_Model {

		function list_user(){
			$this->db->select('user.*,jabatan.*');
			$this->db->join('jabatan','jabatan.id_jabatan=user.id_jabatan');
			$this->db->not_like('akses_default','Admin');
			return $this->db->get('user');
		}
	
		function create($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function trash($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}

		function search($keyword,$table){
			$this->db->select('user.*,jabatan.*');
			$this->db->join('jabatan','jabatan.id_jabatan=user.id_jabatan');
			$this->db->like('nama_user',$keyword);
			$this->db->or_like('username',$keyword);
			return $this->db->get($table);	
		}
	}
	
	/* End of file M_user.php */
	/* Location: ./application/models/M_user.php */
?>
<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_kerusakan extends CI_Model {
	
		function list_kerusakan(){
			$this->db->select('work_order.*,detail_work_order.*,user.*,jenis.*,lokasi.*');
			$this->db->join('detail_work_order','detail_work_order.id_order=work_order.id_order');
			$this->db->join('user','user.id_user=work_order.id_user');
			$this->db->join('jenis','jenis.id_jenis=work_order.id_jenis');
			$this->db->join('lokasi','lokasi.id_lokasi=work_order.id_lokasi');
			return $this->db->get('work_order');
		}

		function list_proses(){
			$this->db->select('work_order.*,detail_work_order.*,user.*,jenis.*,lokasi.*');
			$this->db->join('detail_work_order','detail_work_order.id_order=work_order.id_order');
			$this->db->join('user','user.id_user=work_order.id_user');
			$this->db->join('jenis','jenis.id_jenis=work_order.id_jenis');
			$this->db->join('lokasi','lokasi.id_lokasi=work_order.id_lokasi');
			$this->db->where('status','Sedang Diproses');
			return $this->db->get('work_order');
		}

		function list_selesai(){
			$this->db->select('work_order.*,detail_work_order.*,user.*,jenis.*,lokasi.*');
			$this->db->join('detail_work_order','detail_work_order.id_order=work_order.id_order');
			$this->db->join('user','user.id_user=work_order.id_user');
			$this->db->join('jenis','jenis.id_jenis=work_order.id_jenis');
			$this->db->join('lokasi','lokasi.id_lokasi=work_order.id_lokasi');
			// $this->db->join('user','user.id_user=detail_work_order.id_user');
			$this->db->where('status','Sudah');
			return $this->db->get('work_order');
		}

		function daftar_teknisi(){
			$this->db->select('work_order.*,detail_work_order.*,user.*');
			$this->db->join('work_order','work_order.id_order=detail_work_order.id_order');
			$this->db->join('user','user.id_user=detail_work_order.id_user');
			return $this->db->get('detail_work_order');
		}

		function list_kerusakan_staff(){
			$this->db->select('work_order.*,detail_work_order.*,jenis.*,lokasi.*');
			$this->db->join('detail_work_order','detail_work_order.id_order=work_order.id_order');
			$this->db->join('jenis','jenis.id_jenis=work_order.id_jenis');
			$this->db->join('lokasi','lokasi.id_lokasi=work_order.id_lokasi');
			$this->db->join('user','user.id_user=work_order.id_user');
			$this->db->where('work_order.id_user',$this->session->userdata('id_user'));
			return $this->db->get('work_order');
		}

		function create_kerusakan($data,$table){
			$this->db->insert($table,$data);
			$id = $this->db->insert_id();
			return (isset($id)) ? $id : FALSE;
		}

		function create_detail($detail,$table){
			$this->db->insert($table,$detail);
		}

		function look_crash($where){
			$this->db->select('work_order.*,detail_work_order.*,user.*,jenis.*,lokasi.*');
			$this->db->join('work_order','work_order.id_order=detail_work_order.id_order');
			$this->db->join('user','user.id_user=work_order.id_user');
			$this->db->join('jenis','jenis.id_jenis=work_order.id_jenis');
			$this->db->join('lokasi','lokasi.id_lokasi=work_order.id_lokasi');
			$this->db->where($where);
			return $this->db->get('detail_work_order');
		}

		function proses($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function clear($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}
	
	}
	
	/* End of file M_kerusakan.php */
	/* Location: ./application/models/M_kerusakan.php */
?>
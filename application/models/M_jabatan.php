<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_jabatan extends CI_Model {
	
		function list_jabatan(){
			return $this->db->get('jabatan');
		}

		function create($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function trash($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}
	
	}
	
	/* End of file M_jabatan.php */
	/* Location: ./application/models/M_jabatan.php */
?>
<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_laporan extends CI_Model {
		
		function data_user($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function data_bug($where,$table){
			$this->db->select('work_order.*,detail_work_order.*,user.*,lokasi.*,jenis.*');
			$this->db->join('work_order','work_order.id_order=detail_work_order.id_order');
			$this->db->join('user','user.id_user=work_order.id_user');
			$this->db->join('lokasi','lokasi.id_lokasi=work_order.id_lokasi');
			$this->db->join('jenis','jenis.id_jenis=work_order.id_jenis');
			$this->db->where($where);
			return $this->db->get($table);
		}
	}
	
	/* End of file M_laporan.php */
	/* Location: ./application/models/M_laporan.php */
?>
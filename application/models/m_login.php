<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_login extends CI_Model {
	
		public function cek_login($where){
			return $this->db->get_where('user',$where);
		}

		function cek_akses($id){
			$this->db->where('id_user',$id);
			return $this->db->get('user');
		}
	
	}
	
	/* End of file m_login.php */
	/* Location: ./application/models/m_login.php */
?>
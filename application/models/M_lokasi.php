<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_lokasi extends CI_Model {
		function list_lokasi(){
			return $this->db->get('lokasi');
		}

		function insert($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function trash($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}
	}
	
	/* End of file M_lokasi.php */
	/* Location: ./application/models/M_lokasi.php */
?>